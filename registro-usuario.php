<?php
    include('database.php');
    $tipo_registro = $_POST['tipo_registro'];
    if ($tipo_registro == "1" ) { // Registro con QR
        $qrcode = $_POST['qrcode'];
        $id_persona = $_POST['id_persona'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('SELECT qrcode FROM punto_control WHERE id_cp =:id_cp;');
        $records->bindParam('id_cp',$id_cp);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        if(!empty(result)){
            $correct_qrcode = $result['qrcode'];
            if ($qrcode == $correct_qrcode) {
                $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
                $records->bindParam('id_persona',$id_persona);
                $records->bindParam('id_cp',$id_cp);
                if($records->execute()) {
                    $last_id = $connection->lastInsertId();
                    $records = $connection->prepare('SELECT DATE_FORMAT(acceso.registered_at,\'%d-%m-%Y\') AS fecha_reg, DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg, punto_control.descripcion FROM acceso,punto_control WHERE acceso.id_cp = punto_control.id_cp AND acceso.id_persona = :id_persona AND acceso.id_acceso = :last_id;');
                    $records->bindParam('id_persona',$id_persona);
                    $records->bindParam('last_id',$last_id);
                    $records->execute();
                    $result = $records->fetch(PDO::FETCH_ASSOC);
                    $res = array(
                    "status" => 202,
                    "message" => "!Registro exitoso!",
                    "hora_reg" => $result['hora_reg'],
                    "fecha_reg" => $result['fecha_reg'],
                    "punto_control" => $result['descripcion'],
                    );
                    echo json_encode($res);
                }
                else {
                    $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'El QR que escaneaste no es valido!');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'El QR que escaneaste no es valido!');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "2" ) { // Registro con credencial
        $matricula = $_POST['matricula'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('SELECT id_persona FROM persona WHERE identificador = :matricula;');
        $records->bindParam('matricula',$matricula);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        if(!empty($result)) {
            $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
            $records->bindParam('id_persona',$result['id_persona']);
            $records->bindParam('id_cp',$id_cp);
            if ($records->execute()) {
                $res = array("status" => 202, "message" => 'Se ha registrado exitosamente el acceso del usuario');
                echo json_encode($res);
            }
            else{
                $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'Parece que este usuario no esta registrado en el sistema!');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "3") { // Registro manual de persona ya registrada
        $id_persona = $_POST['id_persona'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
        $records->bindParam('id_persona',$id_persona);
        $records->bindParam('id_cp',$id_cp);
        if ($records->execute()) {
            $res = array("status" => 202, "message" => 'Se ha registrado exitosamente el acceso del usuario');
            echo json_encode($res);
        }
        else {
            $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "4") { // Registro manual de persona externa
        $externo_nombre = $_POST['externo_nombre'];
        $externo_email = $_POST['externo_email'];
        $externo_tel = $_POST['externo_tel'];
        $externo_motivo = $_POST['externo_motivo'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('INSERT INTO persona(email,nombre,tipo_persona) VALUES(:externo_email,:externo_nombre,5);');
        $records->bindParam('externo_email',$externo_email);
        $records->bindParam('externo_nombre',$externo_nombre);
        if ($records->execute()) {
            $last_id = $connection->lastInsertId();
            $records = $connection->prepare('INSERT INTO invitado(id_invitado,telefono,motivo_visita) VALUES(:id_persona,:telefono,:motivo_visita);');
            $records->bindParam('id_persona',$last_id);
            $records->bindParam('telefono',$externo_tel);
            $records->bindParam('motivo_visita',$externo_motivo);
            if ($records->execute()) {
                $records = $connection->prepare('UPDATE persona SET identificador = :last_id WHERE id_persona = :last_id;');
                $records->bindParam('last_id',$last_id);
                $records->execute();
                $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
                $records->bindParam('id_persona',$last_id);
                $records->bindParam('id_cp',$id_cp);
                $records->execute();
                $res = array("status" => 202, "message" => 'Se ha registrado exitosamente el acceso del usuario');
                echo json_encode($res);
            }
            else {
                $res = array("status" => 404,"message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                echo json_encode($last_id);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'No se reconoce el tipo de registro que intentas realizar!');
            echo json_encode($res);
        }
    }
?>