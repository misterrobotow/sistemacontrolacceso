<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="favicon.png"/>
    <link rel="stylesheet" href="css/index-style.css">
    <title>Sistema de control de acceso FI UAEMex</title>
</head>
<body id="page-top">
    <div class="container bg-light" id="topbar">
        <header class="head">
            <nav class="navbar navbar-expand-lg navbar-light head__custom-nav">
                <a class="navbar-brand d-flex flex-row" href="" id="main-title">
                    <img class="" src="img/png/fi_uaem_logo.png" alt="Facultad de Ingenieria UAEM" height="48">&nbsp;
                    <span class="d-none d-sm-block pt-2">&nbsp; Facultad de Ingeniería</span>
                    <span class="d-block d-sm-none pt-2">&nbsp; FI UAEM</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" style="border: none;">
                    <ion-icon name="chevron-down-outline" style="color: black; font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>                        
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item pt-2 mr-3" style="border-bottom: 2px solid #1C3D14;">
                            <a class="nav-link" href="" style="color:#1C3D14;"><strong>Inicio</strong></a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="#about">Acerca de</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="#about">Ayuda</a>
                        </li>
                        <li class="nav-item d-none d-lg-block d-xl-block pt-2">
                            <a class="btn btn-login-nav" href="login.php">Ingresar</a>
                        </li>
                        <li class="nav-item d-none d-lg-block d-xl-block pt-2">
                            <a href="">
                                <img class="" height="50" width="165" src="img/png/img_logo_uaem18-21.png" alt="Universidad Autonoma del Estado de Mexico">
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>
    
    <div class="container bg-light" id="mainContainer">
        <div class="row custom-section bg-light">
            <div class="col-md-7 mb-4 animate__animated animate__pulse" id="main-text-header">
                    <h2 class="mt-0 mb-4" style="text-align:center;font-size:36px">Bienvenido al <strong style="color:#1C3D14;">Sistema de Control de Acceso de la Facultad de Ingeniería</strong> de la UAEM</h2>
                    <p style="text-align:center">Tu seguridad y la de todos dentro de las instalaciones es nuestra prioridad.</p>
                    <div class="d-flex justify-content-center mt-4">
                        <a class="btn btn-login d-flex align-items-center" href="login.php"><span style="vertical-align:middle;">Ingresar</span></a>
                        <a class="btn btn-more d-flex align-items-center" href="#about"><span style="vertical-align:middle;">Saber más...</span></a>
                    </div>
            </div>
            <div class="col-md-5 mb-2 mt-4 img-content">
                <img class="main-img" src="img/png/checking-temperature.jpg" alt="Usa mascarilla para prevenir COVID-19">
            </div>
        </div>
    </div>
    
    <div class="container">
        <h3 class="mb-5" style="text-align:center; color:#333333;"><strong>Formas de acceder a las instalaciones de la Facultad de Ingeniería</strong></h3>
        <p class="mb-5" style="font-size:18px;text-align:center;">
            Con el fin de mantener la seguridad de todas las personas dentro de las
            instalaciones y prevenir posibles contagios de COVID-19, debes registrar tu acceso a las instalaciones
            de la Facultad de Ingeniería. Puedes realizar esta acción de alguna de la siguientes maneras:
        </p>
        <div class="row mb-5">
            <div class="col-lg-4 mb-4">
                <div class="card shadow h-100" style="border:none; border-top: solid 5px #1C3D14;">
                    <div class="d-flex justify-content-center bg-green-strong" style="height:150px;">
                        <img class="" src="img/svg/qr-code.svg" height="" width="100" alt="Descarga la app FI UAEM">
                    </div>
                    <div class="card-body d-flex justify-content-center flex-column">
                        <h4 class="card-title" style="text-align:center;"><strong>Usa la app:</strong></h4>
                        <h4 style="text-align:center;">
                            <img class="mr-1" src="img/png/FI_logo.png" width="44" height="44" alt=" APP FI UAEM">
                            <strong>FI UAEM</strong>
                        </h4>
                        <p class="" style="text-align:center;">
                            Descarga la app FI UAEM e inicia sesion en ella con tu cuenta institucional.
                            Acercate a alguno de los filtros de sanitizacion y una vez que se te tome la temperatura y se te permita pasar,
                            escannea con la app, el codigo QR de ese fitro de sanitizacion.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card shadow h-100" style="border:none; border-top: solid 5px #1C3D14;">
                    <div class="d-flex justify-content-center bg-green-strong" style="height:150px;">
                        <img class="mt-2" src="img/svg/id-card1.svg" height="110" width="120" alt="Usa tu credencial para acceder">
                    </div>
                    <div class="card-body d-flex justify-content-center flex-column">
                        <h4 class="card-title" style="text-align:center;"><strong>Usa tu credencial institucional</strong></h4>
                        <p class="" style="text-align:center;">
                            Acercate a alguno de los filtros de sanitizacion y una vez que se te tome la temperatura y se te permita pasar,
                            proporiona tu credencial institucional al encargado del filtro de sanitizacion para que registre tu acceso.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card shadow h-100" style="border:none; border-top: solid 5px #1C3D14;">
                    <div class="d-flex justify-content-center bg-green-strong" style="height:150px;">
                        <img class="" src="img/svg/thermometer.svg" height="" width="110" alt="Accede con registro manual">
                    </div>
                    <div class="card-body d-flex justify-content-center flex-column">
                        <h4 class="card-title" style="text-align:center;"><strong>Registro manual</strong></h4>
                        <p style="text-align:center;">
                            Acercate a alguno de los filtros de sanitizacion y una vez que se te tome la temperatura y se te permita pasar,
                            proporciona al encargado del punto de sanitizacion tu nombre, tu numero de cuenta o tu numero de empleado para que registre tu acceso.
                            Si eres un usuario externo y deseas ingresar a la Facultad, deberas hacerlo de esta manera y proporcionar algunos datos personales para que se registre tu acceso.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <h5 class="mb-5" style="color:#333333; text-align:center;">
            <strong>
                Recuerda seguir las siguientes medidas de seguridad dentro de las instalaciones
                de la Facultad de Ingeniería:
            </strong>
        </h5>
        <div class="row my-5">
            <div class="col-md-4">
                <div class="d-flex justify-content-center flex-column">
                    <img class="mx-auto mb-2" src="img/svg/050-face-mask.svg"  width="90" height="90" alt="Usa mascarilla o careta en todo momento">
                    <p style="text-align:center;">Usa mascarilla o careta en todo momento</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="d-flex justify-content-center flex-column">
                    <img class="mx-auto mb-2" src="img/svg/024-washing-hands.svg"  width="90" height="90" alt="Lava tus manos frecuentemente">
                    <p style="text-align:center;">Lava tus manos frecuentemente</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="d-flex justify-content-center flex-column">
                    <img class="mx-auto mb-2" src="img/svg/004-social-distancing.svg"  width="90" height="90" alt="Manten el distanciamiento social">
                    <p style="text-align:center;">Manten el distanciamiento social</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Footer -->
    <footer class="" style="background:#333333EF">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-4">
                    <div class="row ml-1 mb-4">
                        <img class="mr-3" src="favicon.png" width="60" height="60" alt="FI UAEM" style="border-radius:4px;">
                        <p class="my-auto text-white" style="font-size:28px; font-weight:600">FI UAEM</p>
                    </div>
                    <h4 class="border-bottom pb-3 border-light text-green-light">Contacto</h4>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="mb-3 footer-item">
                            <a href="https://www.google.com.mx/maps/place/Facultad+de+Ingenieria,Universidad+Aut%C3%B3noma+del+Estado+de+M%C3%A9xico/@19.2838166,-99.6788371,17z/data=!3m1!4b1!4m5!3m4!1s0x85cd89b4fab5eacb:0xdae2eb9ae00d8918!8m2!3d19.2838115!4d-99.6766484">
                                <i class="fas fa-map-marker-alt fa-fw"></i>    
                                &nbsp; Facultad de Ingeniería UAEM. Cerro de Coatepec S/N Ciudad Universitaria C.P: 50100. Toluca, Estado de México.
                            </a>
                        </li>
                        <li class="mb-2 footer-item">
                            <a class="" href="tel:7222140855"><i class="fa fa-phone fa-fw"></i>&nbsp; Tels.:(722) 214 08 55 y 214 07 95</a>
                        </li>
                        <li class="mb-1 footer-item">
                            <a class="" href="mailto:info@company.com"><i class="fa fa-envelope fa-fw"></i>&nbsp; sca.fi.uaem@uaemex.mx</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h4 class="text-green-light border-bottom pb-3 border-light">Sitios de interés</h4>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="footer-item"><a class="" href="http://web.uaemex.mx/avisos/Aviso_Privacidad.pdf"><i class="fas fa-external-link-square-alt"></i> &nbsp; Aviso de privacidad</a></li>
                </div>

                <div class="col-md-4 pt-5">
                    <h4 class="text-green-light border-bottom pb-3 border-light">Información del sitio</h4>
                    <ul class="list-unstyled text-white">
                        <li class="mb-1 footer-item"><a class="" href="#page-top"><i class="fas fa-home"></i>&nbsp; Inicio</a></li>
                        <li class="mb-1 footer-item"><a class="" href="login.php"><i class="fas fa-user-circle"></i>&nbsp; Iniciar sesión</a></li>
                        <li class="mb-1 footer-item"><a class="" href=""><i class="fas fa-info-circle"></i>&nbsp; Acerca de</a></li>
                        <li class="mb-1 footer-item"><a class="" href=""><i class="fas fa-question-circle"></i>&nbsp; Ayuda</a></li>
                    </ul>
                </div>

            </div>

            <div class="row text-white mb-3">
                <div class="col-12 mb-2">
                    <div class="w-100 my-2 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item text-center footer-item">
                            <a class="" href="https://www.facebook.com/FIUAEM/"><ion-icon name="logo-facebook"  style="font-size: 37px; vertical-align: middle;"></ion-icon></a>
                        </li>
                        <li class="list-inline-item text-center footer-item">
                            <a class="" href=""></ion-icon><ion-icon name="logo-twitter" style="font-size: 37px; vertical-align: middle;"></ion-icon></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="w-100 bg-black pt-2">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-white">
                            "2021, Celebración de los 65 años de la Universidad Autónoma del Estado de México"
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script src="js/index-func.js"></script>
</body>
</html>