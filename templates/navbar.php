<?php
  include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/database.php');
  date_default_timezone_set('America/Tegucigalpa');
  $date = date('Y-m-d');
  $fecha = date('d/m/Y');
  $records = $connection->prepare('SELECT DATE_FORMAT(acceso.registered_at,\'%d/%m/%Y\') AS fecha_reg, DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg, punto_control.descripcion FROM acceso,punto_control WHERE acceso.id_cp = punto_control.id_cp AND acceso.id_persona = :id_persona AND acceso.registered_at >= :fecha_actual;');
  $records->bindParam('id_persona',$_SESSION['id_usuario']);
  $records->bindParam('fecha_actual',$date);
  $records->execute();
  $result = $records->fetch(PDO::FETCH_ASSOC);
  if(empty($result))
  {
    $message = "No has pasado por ningun punto de control";
  }
  else {
    $message = "Tienes acceso";
  }
?>
<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top" style="box-shadow: 0 4px 4px 0 #dfe6e9;">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link p-0 m-0 d-md-none">
      <ion-icon name="menu-outline"  class="mx-auto" style="font-size: 27px; color: #2d3436; padding-top:5px;"></ion-icon>
    </button>

    <div class="ml-4" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
      <img src="/sistemaAccesoFI/img/png/FI_logo.png" alt="FI UAEM" width="35" height="35" class="" style="vertical-align:middle;">
      <h3 class="d-none d-lg-inline" style="font-size:18px; font-weight:720; font-style:bold; color:#333333; vertical-align:middle;"> &nbsp; Sistema de Control de Acceso FI UAEM</h3>
      <h1 class="d-inline d-lg-none" style="font-size:22px; font-weight:750; font-style:bold; color:#333333; vertical-align:middle;"> &nbsp; FI UAEM</h1>
    </div>

    <!-- Topbar Search -->
    <form class="d-none form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Buscar aqui..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button" style="background: #1C3D14; border: none;">
            <ion-icon name="search-outline" style="font-size: 20px; color:white; vertical-align: middle;"></ion-icon>
          </button>
        </div>
      </div>
    </form>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Nav Item - Search Dropdown (Visible Only XS) -->
      <li class="nav-item dropdown no-arrow d-none">
        <a class="nav-link dropdown-toggle" href="" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="search-outline" style="font-size: 20px; color:#2d3436; vertical-align: middle;"></ion-icon>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
          <form class="form-inline mr-auto w-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control small" placeholder="Buscar aquí" aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button" style="background: #1C3D14; border: none;">
                  <ion-icon name="search-outline" style="font-size: 20px; color:white; vertical-align: middle;"></ion-icon>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Nav Item - Alerts -->
      <li class="nav-item dropdown no-arrow mx-1 d-none">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="notifications-outline" style="font-size: 25px; color: #5a5c69;"></ion-icon>
          <!-- Counter - Alerts -->
          <span class="badge badge-danger badge-counter">3+</span>
        </a>
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
          <h6 class="dropdown-header">
            Alerts Center
          </h6>
          <!--
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="mr-3">
              <div class="icon-circle bg-primary">
                <i class="fas fa-file-alt text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500">December 12, 2019</div>
              <span class="font-weight-bold">A new monthly report is ready to download!</span>
            </div>
          </a>
          -->
          <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
        </div>
      </li>

      <!-- Nav Item - Messages -->
      <li class="nav-item dropdown no-arrow mx-1 d-none">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="mail-outline" style="font-size: 25px; color: #5a5c69;"></ion-icon>
          <!-- Counter - Messages -->
          <span class="badge badge-danger badge-counter">7</span>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
          <h6 class="dropdown-header">
            Message Center
          </h6>
          <!--
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
              <div class="status-indicator bg-success"></div>
            </div>
            <div class="font-weight-bold">
              <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
              <div class="small text-gray-500">Emily Fowler · 58m</div>
            </div>
          </a>
          -->
          <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
        </div>
      </li>

      <div class="topbar-divider d-none d-sm-block"></div>
      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow" data-toggle="tooltip" data-placement="bottom" title="<?=$message?>">
        <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline" style="color:#333333; font-size:15px;"><?= $_SESSION['username']?></span>
          <ion-icon name="person-circle-outline" style="color:#444444;font-size:26px;"></ion-icon>
          <?php if(!empty($result)): ?>
            <span class="badge badge-pill badge-success badge-counter"><i class="fas fa-check" style="font-size:11px;"></i></span>
          <?php else: ?>
            <span class="badge badge-pill badge-danger badge-counter">-</span>
          <?php endif; ?>
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

          <a class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="Tu perfil">
            <ion-icon name="person-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Perfil
          </a>
          <a class="dropdown-item" data-toggle="modal" data-target="#statusModal">
            <ion-icon name="bookmark-outline"  style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Tu estatus &nbsp;
            <?php if(!empty($result)): ?>
              <span class="badge badge-pill badge-success">Con acceso</span>
            <?php else: ?>
              <span class="badge badge-pill badge-danger">Sin acceso</span>
            <?php endif; ?>
          </a>
          <a class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="Ayuda">
            <ion-icon name="help-circle-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Ayuda
          </a>
          <div class="dropdown-divider"></div>
          <div data-toggle="tooltip" data-placement="bottom" title="Cerrar sesión">
            <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
              <ion-icon name="log-out-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
              Cerrar sesión
            </a>
          </div>
        </div>
      </li>
      <div class="topbar-divider d-none d-md-inline"></div>
      <li class="nav-item d-none d-md-inline" style="padding-top:10px">
        <a href="" class="d-flex flex-row">
          <img class="d-inline mr-1" src="/sistemaAccesoFI/img/svg/facultad-de-ingenieria.svg" alt="Facultad de Ingenieria UAEM" height="30" style="margin-top:12px;">
          <img class="d-inline" src="/sistemaAccesoFI/img/png/Fi_uaem_logo.png" alt="Facultad de Ingenieria UAEM" width="120" height="50">
        </a>
      </li>
    </ul>
</nav>
<!-- End of Topbar -->

<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tu estatus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
      <?php if(!empty($result)): ?>
        <h4 class="text-success mb-3"><strong>Con acceso</strong></h4>
        <img src="/sistemaAccesoFI/img/png/tick.png" alt="Tienes acceso" width="150" height="150">
        <p class="m-0 p-0 mt-4"><strong>Hora de registro:</strong> <?=$result['hora_reg']?></p>
        <p class="m-0 p-0"><strong>Fecha de registro:</strong> <?=$result['fecha_reg']?></p>
        <p class="m-0 p-0"><strong>Punto de control:</strong> <?=$result['descripcion']?></p>
      <?php else: ?>
        <h4 class="text-danger mb-3"><strong>Sin acceso</strong></h4>
        <img src="/sistemaAccesoFI/img/png/wrong.png" alt="Sin acceso" width="150" height="150">
        <p class="m-0 p-0 mt-4"><strong>No has pasado por ningún punto de control</strong></p>
      <?php endif; ?>
      </div>
      <div class="modal-footer">
          <button class="btn btn-hov" type="cancel" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>